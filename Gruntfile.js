/**
 * Created by Jithu.jose on 1/29/2016.
 */
//var path = require('path');
module.exports = function(grunt){

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);
    grunt.loadNpmTasks('grunt-express-server');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        express: {
            options: {
                port: process.env.PORT || 9000
            },
            dev: {
                options: {
                    script: 'server.js',
                    keepalive: true,
                    serverreload: true
                }
            },
            prod: {
                options: {
                    script: 'dist/server.js',
                    node_env: 'production'
                }
            }
        },
        watch: {
            express: {
                files: [
                    'server.js',
                    'backend/**/*.{js,json}',
                    '!backend/**/*Test.js'
                ],
                tasks: ['express:dev', 'wait'],
                options: {
                    livereload: true,
                    nospawn: true //Without this option specified express won't be reloaded
                }
            }
        }
    });

    grunt.registerTask('serve', function (target) {
        grunt.task.run([
            'express:dev',
            'watch'
        ]);
    });
}
