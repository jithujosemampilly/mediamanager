/**
 * Created by Jithu.jose on 2/1/2016.
 */
var path = require('path');
var profileModel = require(path.resolve('backend/database/profileModel'));
var $q = require('q');
var rs = require('random-strings');

function getAllProfilesForSingleProject(projectId){
    var deferred = $q.defer();
    profileModel.find({projectId: projectId}, function(err, profiles){
        if(err){
            deferred.reject(err);
        }
        else{
            deferred.resolve(profiles);
        }
    });
    return deferred.promise;
}
function getSingleProfile(profileId){
    var deferred = $q.defer();
    profileModel.find({id: profileId}, function(err, result){
        if(err || !result.length){
            deferred.reject(err);
        }
        else{
            deferred.resolve(result[0]);
        }
    });
    return deferred.promise;
}

function saveProfile(config){
    var deferred = $q.defer();
    var obj = {
        name: config.profile.name,
        id: rs.numeric(20),
        description: config.profile.description,
        projectId: config.projectId
    }
    var profileObj = new profileModel(obj);
    profileObj.save(function(err){
        if(err){
            deferred.reject(err);
        }
        else{
            deferred.resolve(obj)
        }
    });
    return deferred.promise;
}

function updateProfile(config){
    var deferred = $q.defer();
    profileModel.update({_id: config.profile._id}, {$set: {
        name: config.profile.name,
        description: config.profile.description
    }}, function(err){
        if(err){
            deferred.reject(err);
        }
        else{
            deferred.resolve(config);
        }
    });
    return deferred.promise;
}

function deleteProfile(config){
    var deferred = $q.defer();
    profileModel.find({_id: config._id}).remove(function(err){
        if(err){
            deferred.reject(err);
        }
        else{
            deferred.resolve(config);
        }
    });
    return deferred.promise;
}

module.exports = {
    getAllProfilesForSingleProject: getAllProfilesForSingleProject,
    getSingleProfile: getSingleProfile,

    saveProfile: saveProfile,
    updateProfile: updateProfile,
    deleteProfile: deleteProfile
}