/**
 * Created by Jithu.jose on 2/8/2016.
 */
var path = require('path');
var configurationService = require(path.resolve('backend/services/configurationService'));
var $q = require('q');

function getProjectConfiguration(config) {
    var deferred =  $q.defer();
    configurationService.getProjectConfiguration(config).then(function (response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}

function saveSection(config) {
    var deferred =  $q.defer();
    configurationService.saveSection(config).then(function (response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}
function updateSection(config) {
    var deferred =  $q.defer();
    configurationService.updateSection(config).then(function(response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}
function deleteSection(config) {
    var deferred =  $q.defer();
    configurationService.deleteSection(config).then(function(response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}

function saveField(config) {
    var deferred =  $q.defer();
    configurationService.saveField(config).then(function (response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}
function updateField(config) {
    var deferred =  $q.defer();
    configurationService.updateField(config).then(function(response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}
function deleteField(config) {
    var deferred =  $q.defer();
    configurationService.deleteField(config).then(function(response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}

function getSingleFieldForProject(config) {
    var deferred =  $q.defer();
    configurationService.getSingleFieldForProject(config).then(function(response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}

function saveAttribute(config) {
    var deferred =  $q.defer();
    configurationService.saveAttribute(config).then(function(response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}

function updateAttribute(config) {
    var deferred =  $q.defer();
    configurationService.updateAttribute(config).then(function(response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}

module.exports = {
    getProjectConfiguration: getProjectConfiguration,

    saveSection: saveSection,
    updateSection: updateSection,
    deleteSection: deleteSection,

    saveField: saveField,
    updateField: updateField,
    deleteField: deleteField,

    getSingleFieldForProject: getSingleFieldForProject,
    saveAttribute: saveAttribute,
    updateAttribute: updateAttribute
}