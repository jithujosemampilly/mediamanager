/**
 * Created by Jithu.jose on 2/1/2016.
 */

var path = require('path');
var profileService = require(path.resolve('backend/services/profileService'));
var $q = require('q');

function saveProfile(config) {
    var deferred =  $q.defer();
    profileService.saveProfile(config).then(function (response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}
function updateProfile(config) {
    var deferred =  $q.defer();
    profileService.updateProfile(config).then(function(response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}
function deleteProfile(config) {
    var deferred =  $q.defer();
    profileService.deleteProfile(config).then(function(response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}

function getAllProfilesForSingleProject(projectId) {
    var deferred =  $q.defer();
    profileService.getAllProfilesForSingleProject(projectId).then(function (response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}
function getSingleProfile(profile) {
    var deferred =  $q.defer();
    profileService.getSingleProfile(profile.profileId).then(function (response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}

module.exports = {
    saveProfile: saveProfile,
    updateProfile: updateProfile,
    deleteProfile: deleteProfile,

    getAllProfilesForSingleProject: getAllProfilesForSingleProject,
    getSingleProfile: getSingleProfile
};