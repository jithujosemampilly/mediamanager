/**
 * Created by Jithu.jose on 2/8/2016.
 */

var path = require('path');
var projectDB = require(path.resolve('backend/database/projectDB'));
var $q = require('q');

function getProjectConfiguration(config) {
    var deferred =  $q.defer();
    projectDB.getProjectConfiguration(config.projectId).then(function (response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}

function saveSection(config) {
    var deferred =  $q.defer();
    projectDB.saveSection(config).then(function (response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}
function updateSection(config) {
    var deferred =  $q.defer();
    projectDB.updateSection(config).then(function(response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}
function deleteSection(config) {
    var deferred =  $q.defer();
    projectDB.deleteSection(config).then(function(response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}

function saveField(config) {
    var deferred =  $q.defer();
    projectDB.saveField(config).then(function (response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}
function updateField(config) {
    var deferred =  $q.defer();
    projectDB.updateField(config).then(function(response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}
function deleteField(config) {
    var deferred =  $q.defer();
    projectDB.deleteField(config).then(function(response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}

function getSingleFieldForProject(config) {
    var deferred =  $q.defer();
    projectDB.getSingleFieldForProject(config).then(function(response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}

function saveAttribute(config) {
    var deferred =  $q.defer();
    projectDB.saveAttribute(config).then(function(response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}

function updateAttribute(config) {
    var deferred =  $q.defer();
    projectDB.updateAttribute(config).then(function(response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}

function deleteAttribute(config) {
    var deferred =  $q.defer();
    projectDB.deleteAttribute(config).then(function(response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}

module.exports = {
    getProjectConfiguration: getProjectConfiguration,

    saveSection: saveSection,
    updateSection: updateSection,
    deleteSection: deleteSection,

    saveField: saveField,
    updateField: updateField,
    deleteField: deleteField,

    getSingleFieldForProject: getSingleFieldForProject,
    saveAttribute: saveAttribute,
    updateAttribute: updateAttribute,
    deleteAttribute: deleteAttribute
}