/**
 * Created by Jithu.jose on 2/1/2016.
 */

var path = require('path');
var profileDB = require(path.resolve('backend/database/profileDB'));
var $q = require('q');

function saveProfile(config) {
    var deferred =  $q.defer();
    profileDB.saveProfile(config).then(function (response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}
function updateProfile(config) {
    var deferred =  $q.defer();
    profileDB.updateProfile(config).then(function(response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}
function deleteProfile(config) {
    var deferred =  $q.defer();
    profileDB.deleteProfile(config).then(function(response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}

function getAllProfilesForSingleProject(projectId) {
    var deferred =  $q.defer();
    profileDB.getAllProfilesForSingleProject(projectId).then(function (response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}

function getSingleProfile(profileId) {
    var deferred =  $q.defer();
    profileDB.getSingleProfile(profileId).then(function (response) {
        deferred.resolve(response);
    }).fail(function (err) {
        deferred.reject(err);
    });
    return deferred.promise;
}

module.exports = {
    saveProfile: saveProfile,
    updateProfile: updateProfile,
    deleteProfile: deleteProfile,

    getAllProfilesForSingleProject: getAllProfilesForSingleProject,
    getSingleProfile: getSingleProfile
};