/**
 * Created by Jithu.jose on 2/9/2016.
 */

'use strict';
angular.module('mediaManager')
    .controller('FieldCtrl', [
        '$q',
        '$scope',
        '$rootScope',
        '$state',
        '$stateParams',
        '$localForage',
        'ConfigurationManager',
        'CreateAttributeForm',
        function($q, $scope, $rootScope, $state, $stateParams, $localForage, ConfigurationManager, CreateAttributeForm){
            function getCurrentSelectedField(){
                var deferred = $q.defer();
                $localForage.getItem('currentSelectedField').then(function(field){
                    if(field && $rootScope.previousState.name === 'project'){
                        field = JSON.parse(field);
                        if(field.id === $stateParams.fieldId){
                            deferred.resolve(field);
                        }
                        else{
                            deferred.reject();
                        }
                    }
                    else{
                        if($stateParams.projectId && $stateParams.fieldId){
                            ConfigurationManager.getSingleFieldForProject(
                                $stateParams.projectId,
                                $stateParams.fieldId
                            ).then(function(field){
                                deferred.resolve(field);
                            }, function(err){
                                deferred.reject(err);
                            })
                        }
                        else{
                            deferred.reject();
                        }
                    }
                });
                return deferred.promise;
            }

            getCurrentSelectedField().then(function(field){
                initValues(field);
            }, function(err){
                console.error('Error in getting single field: ', err);
            });


            function initValues(field){
                $scope.field = JSON.parse(JSON.stringify(field));
                $scope.tabs = [
                    {
                        id: 'general',
                        name: 'General',
                        onClick: changeTab
                    },
                    {
                        id: 'parameters',
                        name: 'Parameters',
                        onClick: changeTab
                    }
                ];
                if(field.attributes){
                    $scope.tabs.push({
                        id: 'attributes',
                        name: 'Attributes',
                        onClick: changeTab
                    })
                }
                $scope.generalProperties = [];
                $scope.parameters = $scope.field.parameters;
                $scope.attributes = $scope.field.attributes;
                delete field.parameters;
                delete field.attributes;
                for(var key in field){
                    $scope.generalProperties.push({
                        name: key,
                        value: field[key]
                    });
                }
            }

            $scope.addNewAttribute = function(){
                var deferred = $q.defer();
                CreateAttributeForm.show({
                    promise: deferred.promise,
                    heading: 'Add New Attribute'
                }, function(attrObj){
                    if(attrObj){
                        attrObj.parentId = $scope.field.id
                        ConfigurationManager.saveAttribute(attrObj, $stateParams.projectId).then(function(response){
                            if(response.data){
                                saveAttributeDone(response.data);
                                deferred.resolve();
                            }
                            else{
                                console.log('Error in saving attribute: ', response);
                                deferred.reject(response);
                            }
                        }, function(err){
                            console.error('Error in saving attribute: ', err);
                            deferred.reject(err);
                        });
                    }
                });
            };
            function saveAttributeDone(fieldObj){
                var attrObj = {
                    name: fieldObj.name,
                    key: fieldObj.key,
                    type: fieldObj.type,
                    id: fieldObj.id
                };
                $scope.attributes.push(attrObj);
            }

            $scope.updateAttribute = function(attribute){
                var deferred = $q.defer();
                CreateAttributeForm.show({
                    promise: deferred.promise,
                    heading: 'Update Attribute',
                    data: attribute
                }, function(attrObj){
                    if(attrObj){
                        attrObj.parentId = $scope.field.id
                        ConfigurationManager.updateAttribute(attrObj, $stateParams.projectId).then(function(response){
                            if(response.data){
                                updateAttributeDone(response.data);
                                deferred.resolve();
                            }
                            else{
                                console.log('Error in saving attribute: ', response);
                                deferred.reject(response);
                            }
                        }, function(err){
                            console.error('Error in saving attribute: ', err);
                            deferred.reject(err);
                        });
                    }
                });
            };
            function updateAttributeDone(attributeObj){
                $scope.attributes.forEach(function(attribute, index){
                    if(attribute.id === attributeObj.id){
                        $scope.attributes[index].name = attributeObj.name
                    }
                });
            }

            $scope.deleteAttribute = function(attribute){
                ConfigurationManager.deleteAttribute({
                    id: attribute.id,
                    parentId: $scope.profile.id
                }, $stateParams.projectId).then(function(response){
                    deleteAttributeDone(response.data);
                }, function(err){

                });
            };
            function deleteAttributeDone(attributeObj){
                var newArr = $scope.attributes.filter(function(attribute){
                    return attribute.id !== attributeObj.id
                });
                $scope.attributes = newArr;
            }

            $scope.navigateToField = function(attribute){
                //$localForage.setItem('currentSelectedField', JSON.stringify(attribute)).then(function(err){
                //
                //});
                $state.go('field', {
                    projectId: $stateParams.projectId,
                    fieldId: attribute.id
                }, {reload: true});
            };

            function changeTab(tab){
                $scope.selectedTab = tab;
            }

        }
    ])