/**
 * Created by Jithu.jose on 2/5/2016.
 */

angular.module('mediaManager')
    .controller('ProfileCtrl', [
        '$q',
        '$scope',
        '$stateParams',
        '$rootScope',
        '$localForage',
        'ProfileManager',
        'CreateForm',
        function($q, $scope, $stateParams, $rootScope, $localForage, ProfileManager, CreateForm){

            $scope.profileId = $stateParams.profileId;

            function getCurrentSelectedProfile(){
                var deferred = $q.defer();
                $localForage.getItem('currentSelectedProfile').then(function(profile){
                    if(profile && $rootScope.previousState.name === 'project'){
                        profile = JSON.parse(profile);
                        if((profile.id === $stateParams.profileId) && (profile.projectId === $stateParams.projectId)){
                            deferred.resolve(profile);
                        }
                        else{
                            deferred.reject();
                        }
                    }
                    else{
                        if($stateParams.profileId && $stateParams.projectId){
                            ProfileManager.getSingleProfile($stateParams.profileId).then(function(profile){
                                deferred.resolve(profile);
                            }, function(err){
                                deferred.reject(err);
                            })
                        }
                        else{
                            deferred.reject();
                        }
                    }
                });
                return deferred.promise;
            }

            getCurrentSelectedProfile().then(function(profile){
                $scope.profile = profile;
            }, function(err){
                console.error('Error in getting selected profile: ', err);
            });
        }
    ]);